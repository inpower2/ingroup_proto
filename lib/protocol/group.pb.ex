defmodule InpowerGroupapi.GetCategoriesRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :user_id, 1, type: :string, json_name: "userId"
  field :page_no, 2, type: :int32, json_name: "pageNo"
  field :per_page, 3, type: :int32, json_name: "perPage"
end

defmodule InpowerGroupapi.CreateCategoryRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :name, 1, type: :string
  field :published, 2, type: :bool
end

defmodule InpowerGroupapi.EditCategoryRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :int32
  field :name, 2, type: :string
  field :published, 3, type: :bool
end

defmodule InpowerGroupapi.DeleteCategoryRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :int32
end

defmodule InpowerGroupapi.DeleteGroupRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :int32
end

defmodule InpowerGroupapi.DeleteGroupResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :group, 1, type: InpowerGroupapi.Group
end

defmodule InpowerGroupapi.GetCategoriesResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :categories, 1, type: InpowerGroupapi.CategoryPaged
end

defmodule InpowerGroupapi.CategoryPaged do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :has_next, 1, type: :bool, json_name: "hasNext"
  field :has_prev, 2, type: :bool, json_name: "hasPrev"
  field :prev_page, 3, type: :int32, json_name: "prevPage"
  field :list, 4, repeated: true, type: InpowerGroupapi.Category
  field :next_page, 5, type: :int32, json_name: "nextPage"
  field :first_page, 6, type: :int32, json_name: "firstPage"
  field :page, 7, type: :int32
end

defmodule InpowerGroupapi.CreateCategoryResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :category, 1, type: InpowerGroupapi.Category
end

defmodule InpowerGroupapi.EditCategoryResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :category, 1, type: InpowerGroupapi.Category
end

defmodule InpowerGroupapi.DeleteCategoryResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :category, 1, type: InpowerGroupapi.Category
end

defmodule InpowerGroupapi.Category do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :int32
  field :name, 2, type: :string
  field :published, 3, type: :bool
end

defmodule InpowerGroupapi.GetMembersRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :page_no, 1, type: :int32, json_name: "pageNo"
  field :per_page, 2, type: :int32, json_name: "perPage"
  field :group_id, 3, type: :string, json_name: "groupId"
end

defmodule InpowerGroupapi.GetMembersResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :members, 1, type: InpowerGroupapi.MembersPaged
end

defmodule InpowerGroupapi.MembersPaged do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :has_next, 1, type: :bool, json_name: "hasNext"
  field :has_prev, 2, type: :bool, json_name: "hasPrev"
  field :prev_page, 3, type: :int32, json_name: "prevPage"
  field :list, 4, repeated: true, type: InpowerGroupapi.Member
  field :next_page, 5, type: :int32, json_name: "nextPage"
  field :first_page, 6, type: :int32, json_name: "firstPage"
  field :page, 7, type: :int32
end

defmodule InpowerGroupapi.GetGroupsRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :page_no, 1, type: :int32, json_name: "pageNo"
  field :per_page, 2, type: :int32, json_name: "perPage"
  field :category_id, 3, type: :int32, json_name: "categoryId"
end

defmodule InpowerGroupapi.GetMyGroupsRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :user_id, 1, type: :string, json_name: "userId"
  field :page_no, 2, type: :int32, json_name: "pageNo"
  field :per_page, 3, type: :int32, json_name: "perPage"
end

defmodule InpowerGroupapi.GetGroupRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :string
end

defmodule InpowerGroupapi.GetMyGroupsResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :groups, 1, repeated: true, type: InpowerGroupapi.Group
end

defmodule InpowerGroupapi.GetGroupsResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :groups, 1, repeated: true, type: InpowerGroupapi.Group
end

defmodule InpowerGroupapi.GetGroupResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :group, 1, type: InpowerGroupapi.Group
end

defmodule InpowerGroupapi.CreateGroupRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :name, 1, type: :string
  field :description, 2, type: :string
  field :user_id, 3, type: :string, json_name: "userId"
  field :created_by, 4, type: :string, json_name: "createdBy"
  field :featured_image, 5, type: :string, json_name: "featuredImage"
  field :visibility, 6, type: :string
  field :category_id, 7, type: :int32, json_name: "categoryId"
  field :status, 8, type: :string
end

defmodule InpowerGroupapi.CreateGroupResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :group, 1, type: InpowerGroupapi.Group
end

defmodule InpowerGroupapi.EditGroupRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :string
  field :name, 2, type: :string
  field :user_id, 3, type: :string, json_name: "userId"
  field :created_by, 4, type: :string, json_name: "createdBy"
  field :featured_image, 5, type: :string, json_name: "featuredImage"
  field :visibility, 6, type: :string
  field :description, 7, type: :string
  field :category_id, 8, type: :int32, json_name: "categoryId"
  field :status, 9, type: :string
end

defmodule InpowerGroupapi.EditGroupResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :group, 1, type: InpowerGroupapi.Group
end

defmodule InpowerGroupapi.Group do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :string
  field :name, 2, type: :string
  field :description, 3, type: :string
  field :visibility, 4, type: :string
  field :members, 5, repeated: true, type: InpowerGroupapi.Member
  field :gallery, 6, repeated: true, type: InpowerGroupapi.Gallery
  field :featured_image, 7, type: :string, json_name: "featuredImage"
  field :category, 8, type: InpowerGroupapi.Category
  field :status, 9, type: :string
end

defmodule InpowerGroupapi.AddMemberRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :user_id, 1, type: :string, json_name: "userId"
  field :group_id, 2, type: :string, json_name: "groupId"
  field :role, 3, type: :string
end

defmodule InpowerGroupapi.AddMemberResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :member, 1, type: InpowerGroupapi.Member
end

defmodule InpowerGroupapi.RemoveMemberRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :user_id, 1, type: :string, json_name: "userId"
  field :group_id, 2, type: :string, json_name: "groupId"
end

defmodule InpowerGroupapi.RemoveMemberResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :member, 1, type: InpowerGroupapi.Member
end

defmodule InpowerGroupapi.Member do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :string
  field :user_id, 2, type: :string, json_name: "userId"
  field :group_id, 3, type: :string, json_name: "groupId"
  field :role, 4, type: :string
  field :status, 5, type: :string
end

defmodule InpowerGroupapi.AddToGalleryRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :image, 1, type: :string
  field :caption, 2, type: :string
  field :group_id, 3, type: :string, json_name: "groupId"
end

defmodule InpowerGroupapi.AddToGalleryResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :gallery, 1, type: InpowerGroupapi.Gallery
end

defmodule InpowerGroupapi.Gallery do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :string
  field :image, 2, type: :string
  field :group_id, 3, type: :string, json_name: "groupId"
  field :caption, 4, type: :string
end

defmodule InpowerGroupapi.JoinGroupRequest do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :user_id, 1, type: :string, json_name: "userId"
  field :group_id, 2, type: :string, json_name: "groupId"
  field :answers, 3, repeated: true, type: InpowerGroupapi.Answer
end

defmodule InpowerGroupapi.Answer do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :question_id, 2, type: :int32, json_name: "questionId"
  field :options, 3, repeated: true, type: InpowerGroupapi.Option
end

defmodule InpowerGroupapi.Option do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :id, 1, type: :int32
end

defmodule InpowerGroupapi.JoinGroupResponse do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :member, 1, type: InpowerGroupapi.Member
end

defmodule InpowerGroupapi.ListResponsePagination do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :groups, 1, type: InpowerGroupapi.ResponsePaged
end

defmodule InpowerGroupapi.ResponsePaged do
  @moduledoc false
  use Protobuf, protoc_gen_elixir_version: "0.11.0", syntax: :proto3

  field :has_next, 1, type: :bool, json_name: "hasNext"
  field :has_prev, 2, type: :bool, json_name: "hasPrev"
  field :prev_page, 3, type: :int32, json_name: "prevPage"
  field :list, 4, repeated: true, type: InpowerGroupapi.Group
  field :next_page, 5, type: :int32, json_name: "nextPage"
  field :first_page, 6, type: :int32, json_name: "firstPage"
  field :page, 7, type: :int32
end

defmodule InpowerGroupapi.GroupService.Service do
  @moduledoc false
  use GRPC.Service, name: "inpower_groupapi.GroupService", protoc_gen_elixir_version: "0.11.0"

  rpc :GetGroups, InpowerGroupapi.GetGroupsRequest, InpowerGroupapi.ListResponsePagination

  rpc :GetMyGroups, InpowerGroupapi.GetMyGroupsRequest, InpowerGroupapi.ListResponsePagination

  rpc :GetGroup, InpowerGroupapi.GetGroupRequest, InpowerGroupapi.GetGroupResponse

  rpc :CreateGroup, InpowerGroupapi.CreateGroupRequest, InpowerGroupapi.CreateGroupResponse

  rpc :EditGroup, InpowerGroupapi.EditGroupRequest, InpowerGroupapi.EditGroupResponse

  rpc :AddToGallery, InpowerGroupapi.AddToGalleryRequest, InpowerGroupapi.AddToGalleryResponse

  rpc :GetMembers, InpowerGroupapi.GetMembersRequest, InpowerGroupapi.GetMembersResponse

  rpc :AddMember, InpowerGroupapi.AddMemberRequest, InpowerGroupapi.AddMemberResponse

  rpc :RemoveMember, InpowerGroupapi.RemoveMemberRequest, InpowerGroupapi.RemoveMemberResponse

  rpc :JoinGroup, InpowerGroupapi.JoinGroupRequest, InpowerGroupapi.JoinGroupResponse

  rpc :DeleteGroup, InpowerGroupapi.DeleteGroupRequest, InpowerGroupapi.DeleteGroupResponse

  rpc :GetCategories, InpowerGroupapi.GetCategoriesRequest, InpowerGroupapi.GetCategoriesResponse

  rpc :CreateCategory,
      InpowerGroupapi.CreateCategoryRequest,
      InpowerGroupapi.CreateCategoryResponse

  rpc :EditCategory, InpowerGroupapi.EditCategoryRequest, InpowerGroupapi.EditCategoryResponse

  rpc :DeleteCategory,
      InpowerGroupapi.DeleteCategoryRequest,
      InpowerGroupapi.DeleteCategoryResponse
end

defmodule InpowerGroupapi.GroupService.Stub do
  @moduledoc false
  use GRPC.Stub, service: InpowerGroupapi.GroupService.Service
end